import createApp from './create-app'
import createStore from './create-store'

let store

const els = [...document.querySelectorAll('[data-server-rendered="true"][data-hydrate="true"]')]

els.forEach(el => {
  const { ssrData } = el.dataset
  const parsedData = JSON.parse(ssrData)

  if (typeof parsedData.store === 'object' && !store) {
    store = createStore(parsedData.store)
  }

  createApp(parsedData, el, store)
})
