export const namespaced = true

export const state = () => ({
  isMenuOpen: false,
  isContactOpen: false,
  isContactTileExpanded: false,
  activeContactMenuIndex: 0,
  isModalOpen: false,
  socialLinks: [
    {
      url: '//www.twitter.com/onenorth',
      icon: 'twitter'
    },
    {
      url: '//www.facebook.com/OneNorthInteractive',
      icon: 'facebook'
    },
    {
      url: '//www.linkedin.com/company/2683073',
      icon: 'linkedin'
    },
    {
      url: '//www.instagram.com/onenorth',
      icon: 'instagram'
    }
  ],
  primaryLinks: [
    {
      name: 'Services',
      url: '/services',
      active: false,
      description: 'Find out what “full-service digital agency” means to us.'
    },
    {
      name: 'Work',
      url: '/work',
      active: false,
      description: 'We’re pretty good at what we do. Let us prove it.'
    },
    {
      name: 'Ideas',
      url: '/blog',
      active: false,
      desciption: 'We have a lot of them. Read our perspective on all things digital.'
    },
    {
      name: 'About',
      url: '/about',
      active: false,
      description: 'Leadership, Clients, Partnerships. Get to know us better.'
    },
    {
      name: 'Careers',
      url: '/careers',
      active: false,
      description: 'We want collaborative, curious and creative talent. Sound like you?'
    }
  ],
  contactTiles: [
    {
      title: 'I\'m trying to ...',
      intro: true,
      index: 0
    },
    {
      title: 'get in touch with my team',
      index: 1
    },
    {
      title: 'join your team',
      index: 2
    },
    {
      title: 'work with you',
      index: 3
    },
    {
      title: 'get your digits',
      index: 4
    },
    {
      title: 'slide into your dm\'s',
      index: 5
    }
  ]
})

export const getters = {
  isMenuOpen: state => state.isMenuOpen,
  isContactOpen: state => state.isContactOpen,
  contactTiles: state => state.contactTiles,
  activeContactMenuIndex: state => state.activeContactMenuIndex,
  isContactTileExpanded: state => state.isContactTileExpanded,
  isModalOpen: state => state.isModalOpen
}

export const mutations = {
  toggleMenu: state => {
    state.isContactOpen = false
    state.isMenuOpen = !state.isMenuOpen
    if (state.isMenuOpen) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = 'visible'
    }
  },
  toggleContact: state => {
    state.isMenuOpen = false
    state.isContactOpen = !state.isContactOpen
    state.activeContactMenuIndex = -1
    if (state.isMenuOpen) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = 'visible'
    }
  },
  toggleContactTile: state => {
    state.isContactTileExpanded = !state.isContactTileExpanded
  },
  setContactTile: (state, payload) => {
    state.isContactTileExpanded = payload.isContactTileExpanded
  },
  setActiveContactMenuIndex: (state, payload) => {
    state.activeContactMenuIndex = payload.activeContactMenuIndex
  },
  closeAll: (state) => {
    state.isMenuOpen = false
    state.isContactOpen = false
  },
  setModalOpen: (state, payload) => {
    state.isModalOpen = payload.isModalOpen
  },
  openContactMenu: (state) => {
    state.isMenuOpen = false
    state.isContactOpen = true
    state.isContactTileExpanded = true
    state.activeContactMenuIndex = 5
  }
}
