import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'

Vue.use(Vuex)

Vue.prototype.getValue = function (obj) {
  if (typeof obj === 'object' && !Array.isArray(obj) && obj !== null) {
    return obj.editable || obj.value
  } else {
    return obj
  }
}

export default (ssrData, el, store) => {
  const app = new Vue({
    store,
    el,
    render: createElement => createElement(App, {
      props: ssrData
    })
  })

  return { app, store }
}
