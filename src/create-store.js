import Vuex from 'vuex'

export default (state) => {
  const req = require.context('', true, /^\.\/store\/.*\.js$/)

  const storeConfig = req.keys()
    .reduce((result, key) => {
      const storeModule = req(key)
      const storeModuleName = key.replace('./store/', '').replace('.js', '')
      const isIndexModule = storeModuleName === 'index'

      if (isIndexModule) {
        return { ...storeModule, ...result }
      } else {
        result.modules = result.modules || {}
        result.modules[storeModuleName] = storeModule
        return result
      }
    }, {})

  const store = new Vuex.Store(storeConfig)

  if (typeof state === 'object') {
    Object.keys(state).forEach((key) => {
      store.state[key] = state[key]
    })
  }

  return store
}
