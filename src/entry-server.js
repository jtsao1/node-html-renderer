import createApp from './create-app'
import createStore from './create-store'

export default (ssrData) => {
  const { app } = ssrData.store
    ? createApp(ssrData, undefined, createStore(ssrData.store))
    : createApp(ssrData)

  return app
}
