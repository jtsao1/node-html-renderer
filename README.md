## Usage with Edgejs

1. Run one of the npm commands to compile the assets
2. Incorporate the ```/src/render-html.js``` file via edge-js.

```csharp
var func = Edge.Func(@"return require('./path/to/src/render-html.js')({ bundlePath: 'C:/path/to/assets/dist/vue-ssr-server-bundle.json', watch: true })");

Console.WriteLine(await func(new {
  component = "Counter",
  prop = new {
    initialValue = 22
  }
}));

// syntax to be confirmed
```
See the [edge-js github page](https://github.com/agracio/edge-js#how-to-integrate-nodejs-code-into-clr-code) for more details


## NPM Commands

```npm run build``` - performs production builds and outputs them to the ```/dist``` directory

```npm run build:storybook``` - creates a static version of storybook and outputs it to the ```/dist/storybook``` directory

```npm run dev:proxy``` - starts a proxy server on port 8080. Settings can be configured in the .env file

```npm run dev:storybook``` - starts storybook on port 3000

```npm run dev``` - starts storybook on port 3000 AND starts proxy server on port 8080

```npm run container``` - log into the container (container must be running before running this command)
