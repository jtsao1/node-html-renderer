const path = require('path')

const outputPath = path.join(__dirname, '/dist/')

module.exports = {
  outputPath,
  storybookOutputPath: path.join(outputPath, 'storybook'),
  publicPath: '/assets/dist/',
  mainJsFileName: 'main.js',
  cssFileName: 'styles.css'
}
