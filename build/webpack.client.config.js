const path = require('path')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.base.config')
const cssConfig = require('./webpack-parts/css-config')
const CopyPlugin = require('copy-webpack-plugin')
const { outputPath } = require('./../project-config')

module.exports = () => {
  const isProduction = process.env.NODE_ENV === 'production'

  const config = {
    entry: path.join(__dirname, '../src/entry-client.js'),
    plugins: [
      new VueSSRClientPlugin()
    ]
  }

  const lintingConfiig = {
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/,
          options: {
            fix: true
          }
        }
      ]
    }
  }

  const copyConfig = {
    plugins: [
      new CopyPlugin([
        {
          from: path.join(__dirname, './../src/static'),
          to: outputPath
        }
      ])
    ]
  }

  const clientCssConfig = cssConfig('client')

  return isProduction
    ? merge(baseConfig(), config, clientCssConfig, copyConfig)
    : merge(baseConfig(), config, clientCssConfig, lintingConfiig)
}
