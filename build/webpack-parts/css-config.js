const path = require('path')
const postcssPresetEnv = require('postcss-preset-env')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const merge = require('webpack-merge')
const { cssFileName } = require('./../../project-config')

module.exports = function (type) {
  const isProd = process.env.NODE_ENV === 'production'
  const isSingleFile = type === 'single-file'

  let loader

  if (isSingleFile) {
    loader = MiniCssExtractPlugin.loader
  } else if (type === 'server') {
    loader = 'vue-style-loader'
  } else if (type === 'client') {
    loader = isProd ? 'null-loader' : 'vue-style-loader'
  } else {
    loader = 'vue-style-loader'
  }

  const config = {
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            loader,
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: () => [
                  postcssPresetEnv({ browsers: 'last 2 versions' })
                ]
              }
            },
            'sass-loader',
            {
              loader: 'sass-resources-loader',
              options: {
                resources: path.join(__dirname, './../../src/assets/styles/sass-resources/manifest.scss')
              }
            }
          ]
        }
      ]
    }
  }

  class OnlyOutputCSSFilePlugin {
    apply (compiler) {
      compiler.hooks.emit.tap('OnlyOutputCSSFilePlugin', (compilation) => {
        const assetNames = Object.keys(compilation.assets)
        assetNames.forEach(assetName => {
          if (!assetName.endsWith('.css')) {
            delete compilation.assets[assetName]
          }
        })
      })
    }
  }

  const singleFileConfig = {
    plugins: [
      new OnlyOutputCSSFilePlugin(),
      new MiniCssExtractPlugin({ filename: '[name]' }),
      new OptimizeCSSAssetsPlugin({})
    ],
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: cssFileName,
            test: mod => mod.constructor.name === 'CssModule',
            chunks: 'all',
            enforce: true
          }
        }
      }
    }
  }

  return isSingleFile
    ? merge(config, singleFileConfig)
    : config
}
