const path = require('path')
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.base.config')
const cssConfig = require('./webpack-parts/css-config')

module.exports = () => {
  const config = {
    entry: path.join(__dirname, '../src/entry-server.js'),
    output: {
      libraryTarget: 'commonjs2'
    },
    target: 'node',
    plugins: [
      new VueSSRServerPlugin()
    ],
    watch: process.env.NODE_ENV !== 'production'
  }

  return merge(baseConfig(), config, cssConfig('server'))
}
