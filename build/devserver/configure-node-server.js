const path = require('path')
const { publicPath, mainJsFileName } = require('./../../project-config')

module.exports = (config = {}, options = {}) => {
  config.before = (app) => {
    // We can leverage the before hook to setup routes to server pages.
    // This logic can be extracted into a folder.
    const renderHtmlPath = path.join(__dirname, '../../dist/render-html')
    const renderHtml = require(renderHtmlPath).render

    app.use((req, res, next) => {
      res.renderHtml = renderHtml
      next()
    })

    app.get('/', async (req, res) => {
      const ssrData = require('./mock-data')

      res.set('content-type', 'text/html')

      const html = await Promise.all(ssrData.map(data => {
        return new Promise(resolve => {
          res.renderHtml((_, result) => resolve(result.html), data)
        })
      }))
        .catch(error => console.log('Error Rendering Html', error))

      res.end(`
        <html>
          <head>
            <link rel="stylesheet" type="text/css" href="" />
          </head>
          <body>
            ${html.join('')}
            <script src="${publicPath}${mainJsFileName}" defer></script>
          </body>
        </html>
      `)
    })
  }

  return config
}
