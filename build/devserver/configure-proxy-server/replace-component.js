const path = require('path')
const cheerio = require('cheerio')
const renderer = require('./../../../dist/render-html')
const { outputPath } = require('./../../../project-config')

const renderHtml = renderer.render

module.exports = async (html) => {
  const $ = cheerio.load(html)
  const components = $('[data-server-rendered="true"][data-ssr-data]')

  const updates = []
  components.each((index, component) => {
    const $component = $(component)
    const ssrData = $component.data('ssr-data')

    if (ssrData) {
      updates.push(new Promise((resolve) => renderHtml(undefined, ssrData)
        .catch(error => `<pre>${error}</pre>`)
        .then(html => $component.replaceWith(html))
        .then(resolve)
      ))
    }
  })

  await Promise.all(updates)
    .then(() => $('body').append(`<script>console.log('%cComponent HTML is being replaced by local components', "background-color: yellow;")</script>`))

  return $.html()
}
