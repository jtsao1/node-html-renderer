const zlib = require('zlib')
const replaceComponent = require('./replace-component')

const setupStringReplacement = (proxy, options = {}) => {
  if (!options.stringReplace) {
    return
  }

  proxy.onProxyRes = (proxyRes, req, res) => {
    const contentType = proxyRes.headers['content-type']
    const isHtml = contentType && contentType.includes('text/html')
    const isGzippped = proxyRes.headers['content-encoding'] === 'gzip'

    proxyRes.headers['cache-control'] = 'no-cache'

    if (!isHtml) {
      return
    }

    const originalWrite = res.write
    const originalEnd = res.end

    res.write = () => {}
    res.end = () => {}

    const modifyHtml = async (html, options) => {
      let modifiedHtml = html || ''

      if (options && Array.isArray(options.stringReplace)) {
        options.stringReplace.forEach(option => {
          modifiedHtml = modifiedHtml.replace(option.pattern, option.replacement)
        })
      }

      if (options && options.replaceComponent) {
        modifiedHtml = await replaceComponent(modifiedHtml)
      }

      return modifiedHtml
    }

    if (isGzippped) {
      let originalBody = Buffer.from([])

      proxyRes.on('data', data => {
        originalBody = Buffer.concat([originalBody, data])
      })

      proxyRes.on('end', async () => {
        const bodyString = zlib.gunzipSync(originalBody).toString('utf8')
        const newBody = await modifyHtml(bodyString, options)
        const newzip = zlib.gzipSync(newBody)

        res.set({ 'content-length': newzip.length })
        originalWrite.call(res, newzip)
        originalEnd.call(res)
      })
    } else {
      let originalBody = ''

      proxyRes.on('data', data => {
        originalBody = originalBody + data
      })

      proxyRes.on('end', async () => {
        const newBody = await modifyHtml(originalBody, options)

        res.set({ 'content-length': newBody.length })
        originalWrite.call(res, newBody)
        originalEnd.call(res)
      })
    }
  }
}

module.exports = (config = {}, options = {}) => {
  config.proxy = {
    context: () => true,
    target: options.target,
    changeOrigin: true
  }

  setupStringReplacement(config.proxy, options)

  return config
}
