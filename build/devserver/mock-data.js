const store = {
  socialLinks: [
    {
      url: {
        value: '//www.twitter.com/onenorth',
        editable: ''
      },
      icon: {
        value: 'twitter',
        editable: ''
      }
    },
    {
      url: {
        value: '//www.facebook.com/OneNorthInteractive',
        editable: ''
      },
      icon: {
        value: 'facebook',
        editable: ''
      }
    },
    {
      url: {
        value: '//www.linkedin.com/company/2683073',
        editable: ''
      },
      icon: {
        value: 'linkedin',
        editable: ''
      }
    },
    {
      url: {
        value: '//www.instagram.com/onenorth',
        editable: ''
      },
      icon: {
        value: 'instagram',
        editable: ''
      }
    }
  ],
  primaryLinks: [
    {
      name: {
        value: 'Services1',
        editable: ''
      },
      url: {
        value: '/services',
        editable: ''
      },
      description: {
        value: 'Find out what “full-service digital agency” means to us.',
        editable: ''
      }
    },
    {
      name: {
        value: 'Work1',
        editable: ''
      },
      url: {
        value: '/work',
        editable: ''
      },
      description: {
        value: 'We’re pretty good at what we do. Let us prove it.',
        editable: ''
      }
    },
    {
      name: {
        value: 'Ideas',
        editable: ''
      },
      url: {
        value: '/blog',
        editable: ''
      },
      desciption: {
        value: 'We have a lot of them. Read our perspective on all things digital.',
        editable: ''
      }
    },
    {
      name: {
        value: 'About',
        editable: ''
      },
      url: {
        value: '/about',
        editable: ''
      },
      description: {
        value: 'Leadership, Clients, Partnerships. Get to know us better.',
        editable: ''
      }
    },
    {
      name: {
        value: 'Careers',
        editable: ''
      },
      url: {
        value: '/careers',
        editable: ''
      },
      description: {
        value: 'We want collaborative, curious and creative talent. Sound like you?',
        editable: ''
      }
    }
  ]
}

module.exports = [
  {
    sitecore: {
      rendering: {
        componentName: 'SiteNav'
      },
      dataSource: {
        store
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'Hero'
      },
      dataSource: {
        image: {
          value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Homepage_Hero-Image_Optimized_12.21.18.jpg',
          editable: ''
        },
        title: {
          value: '<p style="text-align: center;">Digital rede<span style="letter-spacing: .25rem;">f</span>ined</p>',
          editable: ''
        },
        message: {
          value: 'TEKsystems Acquires One North',
          editable: ''
        },
        introText: {
          value: 'Breakthrough brands create powerful digital experiences by seamlessly integrating strategy, experience design and technology. Our unique approach, perspective and holistic blend of capabilities empowers the world’s most complex organizations to stand out and deliver results.',
          editable: ''
        },
        services: [
          {
            title: {
              value: 'Digital Strategy',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/teams/digital-strategy',
              editable: ''
            },
            capabilities: [
              {
                value: 'Strategic Planning',
                editable: ''
              },
              {
                value: 'Technology Consulting',
                editable: ''
              },
              {
                value: 'Data & Optimization Strategy',
                editable: ''
              }
            ]
          },
          {
            title: {
              value: 'Brand & Experience Design',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/teams/digital-strategy',
              editable: ''
            },
            capabilities: [
              {
                value: 'Brand Research & Strategy',
                editable: ''
              },
              {
                value: 'Content Strategy',
                editable: ''
              },
              {
                value: 'UX Strategy & Design',
                editable: ''
              },
              {
                value: 'Visual & Interaction Design',
                editable: ''
              }
            ]
          },
          {
            title: {
              value: 'Technology',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/teams/digital-strategy',
              editable: ''
            },
            capabilities: [
              {
                value: 'Web Development',
                editable: ''
              },
              {
                value: 'Mobile & Custom App Development',
                editable: ''
              },
              {
                value: 'Hosting & Application Management',
                editable: ''
              },
              {
                value: 'Data Integration',
                editable: ''
              }
            ]
          }
        ]
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'TileGrid'
      },
      dataSource: {
        tiles: [
          {
            logo: {
              value: 'https://onenorth.blob.core.windows.net/keystone/skadden-black.svg',
              editable: ''
            },
            image: {
              value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Skadden_Hero_Work-Landing.jpg',
              editable: ''
            },
            color: {
              value: '#ed3124',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/work#skadden',
              editable: ''
            }
          },
          {
            logo: {
              value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/plante-black.png',
              editable: ''
            },
            image: {
              value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Plante-Moran-PWA_Hero_Work-Landing.jpg',
              editable: ''
            },
            color: {
              value: '#dc6026',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/work#plante-moran-1',
              editable: ''
            }
          },
          {
            logo: {
              value: 'https://onenorth.blob.core.windows.net/keystone/gt-black.svg',
              editable: ''
            },
            image: {
              value: 'https://tempmigration.blob.core.windows.net/onenorth/GT_Hero_Work-Landing.jpg',
              editable: ''
            },
            color: {
              value: '#be9b39',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/work#greenberg-traurig',
              editable: ''
            }
          },
          {
            logo: {
              value: 'https://onenorth.blob.core.windows.net/keystone/burns-black.png',
              editable: ''
            },
            image: {
              value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Burns_Hero_Work-Landing_New-from-Nate.jpg',
              editable: ''
            },
            color: {
              value: '#0056b9',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/work#burns-and-mcdonnell',
              editable: ''
            }
          },
          {
            logo: {
              value: 'https://onenorth.blob.core.windows.net/keystone/navigant-black.svg',
              editable: ''
            },
            image: {
              value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Navigant_Hero_Work-Landing.jpg',
              editable: ''
            },
            color: {
              value: '#65a337',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/work#navigant',
              editable: ''
            }
          },
          {
            logo: {
              value: 'https://onenorth.blob.core.windows.net/keystone/sidley-black.svg',
              editable: ''
            },
            image: {
              value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Sidley_Hero_Work-Landing.jpg',
              editable: ''
            },
            color: {
              value: '#005e85',
              editable: ''
            },
            link: {
              value: 'https://www.onenorth.com/work#sidley',
              editable: ''
            }
          }
        ]
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'SlidingDoors'
      },
      dataSource: {
        featuredItem1: {
          title: {
            value: 'Ideas',
            editable: ''
          },
          header: {
            value: 'How Digital Brand Experiences Transform PSOs and Drive Sustainable Growth',
            editable: ''
          },
          description: {
            value: 'Are there certain characteristics or traits that high-performing PSOs embody that makes their success look effortless and elegant – traits that underperformers fail to adopt? We think so! High-performing PSOs understand and are able to act on three important and interdependent realities that underperformers tend to struggle with: they lead with brand; they are laser focused on building a remarkable, engaging experience; and they recognize that having a digital engine is critical to their success.',
            editable: ''
          },
          buttons: [
            {
              url: {
                value: 'https://www.onenorth.com/blogs',
                editable: ''
              },
              text: {
                value: 'read this post',
                editable: ''
              }
            },
            {
              url: {
                value: 'https://www.onenorth.com/blogs',
                editable: ''
              },
              text: {
                value: 'view all posts',
                editable: ''
              }
            }
          ]
        },
        featuredItem2: {
          title: {
            value: 'About',
            editable: ''
          },
          header: {
            value: 'Differentiated Digital Experiences',
            editable: ''
          },
          description: {
            value: 'Branded digital experiences not only help organizations stand out in competitive markets, but ultimately win new business, strengthen client relationships, and attract and retain top talent. Our experts use digital to reimagine how clients connect, communicate and engage with their audiences.',
            editable: ''
          },
          buttons: [
            {
              url: {
                value: 'https://www.onenorth.com/about',
                editable: ''
              },
              text: {
                value: 'learn more',
                editable: ''
              }
            }
          ]
        }
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'NextUp'
      },
      dataSource: {
        title: {
          value: 'Let’s Work Together',
          editable: ''
        },
        text: {
          value: 'Whether we’d be an extension of your team, or you’re interested in joining ours, let’s get to know each other.',
          editable: ''
        },
        links: [
          {
            url: {
              value: 'https://www.onenorth.com/services',
              editable: ''
            },
            text: {
              value: 'explore our services',
              editable: ''
            }
          },
          {
            url: {
              value: 'https://www.onenorth.com/careers',
              editable: ''
            },
            text: {
              value: 'view open positions',
              editable: ''
            }
          }
        ]
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'FooterComponent'
      },
      dataSource: {
        address: {
          value: '<p>One North Interactive&nbsp;<br>222 North LaSalle St, #1500 <br>Chicago, IL 60601</p>',
          editable: ''
        },
        phoneNumber: {
          value: '+1 312.469.1740',
          editable: ''
        },
        store
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'DetailHero'
      },
      dataSource: {
        title: {
          value: 'How Digital Brand Experiences Transform PSOs and Drive Sustainable Growth',
          editable: ''
        },
        date: 'March 04, 2019',
        author: {
          text: {
            value: 'Sheryl Kravitz',
            editable: ''
          },
          url: {
            value: 'https://www.onenorth.com/blog/?authors=5bd1de01118e86002506f38c',
            editable: ''
          }
        },
        image: {
          value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Anna-Kerinina.jpg',
          editable: ''
        }
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'DetailBody'
      },
      dataSource: {
        content: {
          value: '<p>Today&rsquo;s professional services market is hyper-competitive. It&rsquo;s no secret that firms across the PSO spectrum &ndash; from accounting, consulting, financial services and law firms to architecture, engineering and construction firms &ndash; are in a sprint to stay ahead of disruption and change. And similar to any other type of B2B industry, within the PSO space exists a select group of marquis firms that are viewed as real leaders in their market, while all others are typically lumped together as not-so-fast followers.</p>\r\n<p>So, what is it about high performers that sets them apart from the pack? Are there certain characteristics or traits that high-performing PSOs embody that makes their success look effortless and elegant &ndash; traits that underperformers fail to adopt? The answer is yes. We refer to this as the Anna Karenina effect, in which Anna, the tragic character in Leo Tolstoy&rsquo;s classic novel, wryly notes: &ldquo;All happy families are alike; each unhappy family is unhappy in its own way.&rdquo; Indeed, we have found that high-performing (happy) PSOs tend to behave very much alike, when compared to their lower performing (unhappy) PSO competitive set.</p>\r\n<p>PSOs that do achieve more do so by marshalling their firm leadership to champion efforts to bridge the divide that otherwise impedes their ability to create meaningful connections with clients. Their efforts and leadership drive performance and stronger results, something that is increasingly being asked of today&rsquo;s marketers. These super-charged, high-performing (happy) PSOs understand and are able to act on three important and interdependent realities that underperformers tend to struggle with: they lead with brand; they are laser focused on building a remarkable, engaging experience; and they recognize that having a digital engine is critical to their success.&nbsp;</p>\r\n<ol>\r\n<li><strong>Lead with Brand</strong><br />High-performing PSOs who lead with brand operate with an understanding that their brand is not just about having a logo, or creating noise in the market or simply marketing. Instead, they view brand as a direct lever that drives a client&rsquo;s willingness to consider, engage and pay for services. They work toward developing their brand story in a way that goes well beyond establishing awareness and credibility around &lsquo;trust.&rsquo; These are the firms that are creating emotive, compelling connections and who recognize how business value combined with personal value establishes a tangible position of privilege in the market. They understand that brand serves a larger purpose in their effort to capture what is true and authentic to who they are and what they stand for. They also communicate how they deliver services in a way that resonates with and entices stakeholders to engage with their firm. As a result, they are able to attract the best talent, grow their client base, build their reputation, drive intense loyalty and set themselves apart from their competitors in ways that unhappy PSOs (market followers) typically tend to struggle with.</li>\r\n<li><strong>Build Experiences, Not Websites</strong><br />PSOs that struggle (or in this case are unhappy) sometimes fail to realize that their website is a symptom of a bigger challenge around how to cope with change and disruption in the digital economy. This can be for a number of different reasons. Sometimes they lack consensus or a proper understanding of the role that digital should play, and how influential it has become in establishing credibility within the PSO space. These types of PSOs (as a whole) tend to hold onto deeply entrenched opinions about how establishing trusted relationships and referrals have little to do with creating an online experience that connects and engages with stakeholders. They view the website as being the equivalent of the yellow pages, and not much more. Whatever the reason or challenge may be, successful, digitally-driven PSOs do not do this. Instead, they view their website as a full-on, interactive experience that not only establishes credibility, but is critical to how the firm manages its reputation, heightens its visibility, attracts talent and serves as a source for new business development. They view their website not just as a supplemental marketing tool, but as a persuasive, hard-working digital engine, rich with data and actionable insights, that directly supports the firm&rsquo;s ability to grow its book of business in addition to managing its reputation.</li>\r\n<li><strong>Develop a Digital Experience Strategy</strong><br />Finally, today&rsquo;s savvy PSO marketers who lead high-performing PSOs wholly embrace the reality that building a website is no longer a fixed, static endeavor. Instead, they recognize that refreshing their website is an ongoing initiative that requires a deep understanding of how the digital branded experience can support long-term business goals. They work to develop a holistic, integrated strategy that can evolve and grow as the firm&rsquo;s needs change. This approach empowers these firms to reach new geographic markets, establish new practices and spheres of influence, and adopt new technologies like progressive web applications, AI and any number of social networking or mobile apps that continue to be quickly adopted and preferred by customers and talent exploring whether the firm is a fit. They form key partnerships across their organizations and collaborate directly with HR, firm leadership, technical operations and other firm disciplines to build and implement an aligned, mutually beneficial digital strategy that drives sustainable performance and results over time.</li>\r\n</ol>\r\n<p>Marketers who are under pressure to amp up performance should consider revisiting their website as an opportunity to transition to a digital brand experience framework. By doing so, they will be better positioned to chart a course that, over time, will provide a better return on their investment and produce stronger results.</p>\r\n<p><em>If you&rsquo;re interested in exploring how your firm can overcome the Anna Karenina effect, <a href=\"https://www.onenorth.com/contact\" target=\"_blank\">let&rsquo;s connect</a>. Our experts can help your firm grow to be a happy and successful PSO &ndash; and stay that way.</em></p>',
          editable: ''
        }
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'Tags'
      },
      dataSource: {
        tagItems: [
          {
            text: {
              value: 'Brand',
              editable: ''
            },
            url: {
              value: 'https://www.onenorth.com',
              editable: ''
            }
          },
          {
            text: {
              value: 'Client Experience',
              editable: ''
            },
            url: {
              value: 'https://www.onenorth.com',
              editable: ''
            }
          },
          {
            text: {
              value: 'Drive Business Revenue',
              editable: ''
            },
            url: {
              value: 'https://www.onenorth.com',
              editable: ''
            }
          },
          {
            text: {
              value: 'Attract & Retain Top Talent',
              editable: ''
            },
            url: {
              value: 'https://www.onenorth.com',
              editable: ''
            }
          },
          {
            text: {
              value: 'Data',
              editable: ''
            },
            url: {
              value: 'https://www.onenorth.com',
              editable: ''
            }
          }
        ]
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'Author'
      },
      dataSource: {
        image: {
          value: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Sheryl-Kravitz_B%26W-Cropped.jpg',
          editable: ''
        },
        name: {
          value: 'Sheryl Kravitz',
          editable: ''
        },
        title: {
          value: 'Director, Brand & Communications',
          editable: ''
        },
        content: {
          value: `<p>Sheryl Kravitz, is Director, Brand and Communications. She has more than 15 years of experience helping highly matrixed companies solve their most pressing brand and communications challenges. Sheryl combines creativity and insight to provide guidance on name development, M&amp;A branding, brand revitalization, portfolio alignment, communications and messaging.</p> <p><strong>Favorite breakfast food</strong>: Coffee and more coffee</p> <p><strong>Favorite Season</strong>: Summer - because the days are longer</p>`,
          editable: ''
        },
        socialLinks: [
          {
            link: {
              value: 'test@onenorth.com',
              editable: ''
            },
            icon: {
              value: 'email',
              editable: ''
            }
          },
          {
            link: {
              value: 'https://www.onenorth.com',
              editable: ''
            },
            icon: {
              value: 'linkedin',
              editable: ''
            }
          }
        ]
      }
    }
  },
  {
    sitecore: {
      rendering: {
        componentName: 'FooterComponent'
      },
      dataSource: {
        address: {
          value: '<p>One North Interactive&nbsp;<br>222 North LaSalle St, #1500 <br>Chicago, IL 60601</p>',
          editable: ''
        },
        phoneNumber: {
          value: '+1 312.469.1740',
          editable: ''
        },
        store
      }
    }
  }
]
