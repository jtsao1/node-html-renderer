const VueLoaderPlugin = require('vue-loader/lib/plugin')
const WebpackBar = require('webpackbar')
const { outputPath, publicPath, mainJsFileName } = require('./../project-config')

module.exports = () => {
  const isProduction = process.env.NODE_ENV === 'production'

  return {
    mode: isProduction ? 'production' : 'development',
    devtool: isProduction ? 'hidden-source-map' : 'eval-source-map',
    output: {
      path: outputPath,
      publicPath: publicPath,
      filename: mainJsFileName,
      chunkFilename: '[name]-[contenthash:6].js'
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader'
        }
      ]
    },
    plugins: [
      new VueLoaderPlugin(),
      new WebpackBar()
    ],
    resolve: {
      extensions: ['.js', '.vue']
    },
    stats: 'minimal'
  }
}
