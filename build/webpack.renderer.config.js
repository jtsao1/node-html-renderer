const path = require('path')
const WebpackBar = require('webpackbar')
const projectConfig = require('./../project-config')

const isProduction = process.env.NODE_ENV === 'production'

module.exports = {
  mode: isProduction ? 'production' : 'development',
  devtool: 'source-map',
  entry: path.join(__dirname, '../build/render-html.js'),
  output: {
    path: projectConfig.outputPath,
    publicPath: projectConfig.publicPath,
    filename: 'render-html.js',
    libraryTarget: 'commonjs2'
  },
  externals: 'fsevents', // fsevents is macspecific
  target: 'node',
  plugins: [
    new WebpackBar()
  ],
  stats: 'none'
}
