const fs = require('fs')
const path = require('path')
const { createBundleRenderer } = require('vue-server-renderer')
const { outputPath } = require('./../project-config')

const isDev = process.env.NODE_ENV !== 'production'

const generateErrorMessage = ({ error, data, errorType = 'Error' }) => {
  const consoleLogMessage = `
    [SSR] ${errorType}
    [SSR] Data Provided:\n${JSON.stringify(data, null, 2)}
    [SSR] Error Message:
    ${error.stack}
  `

  const htmlMessage = `
    <p style="font-weight: bold;">${errorType}</p>
    <p>Data Provided</p>
    <pre>${JSON.stringify(data, null, 2)}</pre>
    <p>[SSR] Error Message</p>
    <pre>${error.stack}</pre>
  `

  const html = `
    <div class="oni-html-renderer-error">
      ${!isDev ? '' : htmlMessage}
      <script>console.log(\`${consoleLogMessage}\`)</script>
    </div>
  `

  return html
}

let renderer

const sitecorePath = path.join(process.cwd(), 'vue-ssr-server-bundle.json')
const devPath = path.join(process.cwd(), outputPath, 'vue-ssr-server-bundle.json')
const bundlePath = fs.existsSync(sitecorePath) ? sitecorePath : devPath

const setupRenderer = () => {
  try {
    const serverBundle = fs.readFileSync(bundlePath, 'utf-8')
    renderer = createBundleRenderer(JSON.parse(serverBundle))
  } catch (error) {
    renderer = {
      renderToString: () => Promise.resolve(generateErrorMessage({
        errorType: 'Renderer Setup Error',
        error,
        data: {
          bundlePath,
          nodeEnv: process.env.NODE_ENV
        }
      }))
    }
  }
}

setupRenderer()

if (isDev) {
  require('chokidar')
    .watch(bundlePath, { ignoreInitial: true })
    .on('change', setupRenderer)
}

const parse = value => {
  let result

  if (typeof value === 'object') {
    result = value
  } else if (typeof value === 'string') {
    try {
      result = JSON.parse(value)
    } catch (error) {
      result = {}
    }
  } else {
    result = {}
  }

  return result
}

const isObject = obj => typeof obj === 'object' && obj !== null

const each = (obj, callback) => {
  if (!isObject(obj)) {
    return
  }

  Object.keys(obj).forEach(key => callback(obj[key], key, obj))
}

const isEditableField = obj => {
  if (!isObject(obj)) {
    return false
  }

  return Object.keys(obj).length === 2 &&
    obj.hasOwnProperty('value') &&
    obj.hasOwnProperty('editable')
}

const mergeEditable = obj => {
  if (!isObject(obj)) {
    return obj
  }

  each(obj, (fieldValue, key, collection) => {
    if (isEditableField(fieldValue)) {
      collection[key] = fieldValue.editable || fieldValue.value
    } else if (isObject(fieldValue)) {
      mergeEditable(fieldValue)
    }
  })

  return obj
}

module.exports = {
  render: (callback, data, viewBag) => {
    const parsedData = parse(data)
    const parsedViewBag = parse(viewBag)

    const componentProps = mergeEditable({
      // ...parsedData,
      ...parsedData.sitecore.dataSource,
      viewBag: parsedViewBag
    })

    const store = componentProps.store
    delete componentProps.store

    const ssrData = {
      component: parsedData.sitecore.rendering.componentName,
      props: componentProps,
      store
      // state : parsedDate.state
    }

    return renderer.renderToString(ssrData)
      .catch(error => generateErrorMessage({
        errorType: 'Error rendering HTML',
        error,
        data: ssrData
      }))
      .then(html => callback(null, {
        html,
        status: 200,
        redirect: null
      }))
  }
}
