const path = require('path')
const merge = require('webpack-merge')
const clientConfig = require('./webpack.client.config')
const configureProxyServer = require('./devserver/configure-proxy-server')
const configureNodeServer = require('./devserver/configure-node-server')
const { publicPath, mainJsFileName, cssFileName } = require('./../project-config')

module.exports = () => {
  const config = {
    devServer: {
      overlay: {
        warnings: true,
        errors: true
      },
      open: true,
      hot: true,
      compress: true,
      port: process.env.DEVSERVER_PORT,
      host: '0.0.0.0',
      contentBase: [
        path.join(__dirname, './../src/static/'),
        path.join(__dirname, './../dist/')
      ]
    }
  }

  const devServerMode = process.env.DEV_SERVER_MODE

  if (devServerMode === 'node') {
    configureNodeServer(config.devServer, {})
  } else if (devServerMode === 'proxy' && process.env.ONI_DEV_PROXY_TARGET) {
    configureProxyServer(config.devServer, {
      target: process.env.ONI_DEV_PROXY_TARGET,
      replaceComponent: process.env.ONI_DEV_PROXY_REPLACE_HTML === 'true',
      stringReplace: [
        {
          pattern: new RegExp(`${publicPath}?\.*/${mainJsFileName}`, 'i'),
          replacement: `${publicPath}${mainJsFileName}`
        },
        {
          pattern: new RegExp(`${publicPath}?\.*/${cssFileName}\.*"`, 'i'),
          replacement: '"'
        }
      ]
    })
  }

  return merge(clientConfig(), config)
}
