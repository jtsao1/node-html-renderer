const path = require('path')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.base.config')
const cssConfig = require('./webpack-parts/css-config')

module.exports = () => {
  const config = {
    entry: path.join(__dirname, '../src/entry-client.js')
  }

  return merge(baseConfig(), config, cssConfig('single-file'))
}
