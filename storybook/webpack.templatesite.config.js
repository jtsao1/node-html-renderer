const path = require('path')
const { outputPath, storybookOutputPath } = require('./../project-config')

module.exports = () => {
  const config = {
    // Dummy entry file
    entry: path.join(__dirname, './../package.json'),
    output: {
      path: outputPath
    },
    devServer: {
      host: '0.0.0.0',
      port: process.env.PORT || 3030,
      compress: true,
      contentBase: [
        path.join(__dirname, './../src/static/'),
        path.join(storybookOutputPath)
      ],
      before (app) {
        if (!process.env.LOGIN_PASSWORD) {
          return
        }

        const cookieName = 'oni-user'

        const getCookie = (cookie) => {
          const nameEQ = cookieName + '='
          const parts = cookie.split('; ')
          const part = parts.filter(part => part.startsWith(nameEQ))[0]
          return part ? part.slice(nameEQ.length, part.length) : null
        }

        const hasValidCookie = (cookie) => {
          const value = getCookie(cookie)
          if (!value) {
            return false
          }
          const decoded = Buffer.from(value, 'base64').toString('ascii')
          return parseInt(decoded) > Date.now()
        }

        const setCookie = (res) => {
          const days = 1
          const expires = Date.now() + (days * 24 * 60 * 60 * 1000)
          const value = Buffer.from(JSON.stringify(expires)).toString('base64')
          res.cookie(cookieName, value, { expires: new Date(expires), path: '/' })
        }

        app.get('/login', (req, res) => {
          const failedAttempt = req.query.authorized === 'false'

          res.end(`
            <html>
              <head>
                <meta name="robots" content="noindex, nofollow">
              </head>
              <body>
                <form action="/login" method="post">
                  <input type="password" required name="password" placeholder="Password">
                  <button type="submit">Submit</button>
                  ${failedAttempt ? 'Incorrect Password' : ''}
                </form>
              </body>
            </html>
          `)
        })

        app.post('/login', (req, res) => {
          let data = ''
          req.on('data', chunk => { data += chunk })
          req.on('end', () => {
            const receivedPassword = data.slice(data.indexOf('=') + 1)

            if (receivedPassword === process.env.LOGIN_PASSWORD) {
              setCookie(res)
              res.redirect('/')
            } else {
              res.redirect('/login?authorized=false')
            }
          })
        })

        app.use('/*', (req, res, next) => {
          if (hasValidCookie(req.headers.cookie)) {
            setCookie(res)
            next()
          } else {
            res.redirect('/login')
          }
        })
      }
    }
  }

  return config
}
