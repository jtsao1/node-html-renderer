const merge = require('webpack-merge')
const cssConfig = require('./../build/webpack-parts/css-config')
const lintingConfig = require('./../build/webpack-parts/linting-config')

module.exports = async ({ config }) => merge(config, cssConfig(), lintingConfig())
