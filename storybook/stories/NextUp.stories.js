import { storiesOf } from '@storybook/vue'
import NextUp from './../../src/components/NextUp.vue'

storiesOf('NextUp', module)
  .add('default view', () => ({
    components: { NextUp },
    data: () => ({
      title: 'Let’s Work Together',
      text: 'Whether we’d be an extension of your team, or you’re interested in joining ours, let’s get to know each other.',
      links: [
        {
          url: 'https://www.onenorth.com/services',
          text: 'explore our services'
        },
        {
          url: 'https://www.onenorth.com/careers',
          text: 'view open positions'
        }
      ]
    }),
    template: `
      <NextUp
        :title="title"
        :text="text"
        :links="links"
      />
    `
  }))
