import { storiesOf } from '@storybook/vue'
import Hero from './../../src/components/Hero.vue'

storiesOf('Hero', module)
  .add('default view', () => ({
    components: { Hero },
    data () {
      return {
        image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Homepage_Hero-Image_Optimized_12.21.18.jpg',
        title: '<p style="text-align: center;">Digital rede<span style="letter-spacing: .25rem;">f</span>ined</p>',
        message: 'TEKsystems Acquires One North',
        introText: 'Breakthrough brands create powerful digital experiences by seamlessly integrating strategy, experience design and technology. Our unique approach, perspective and holistic blend of capabilities empowers the world’s most complex organizations to stand out and deliver results.',
        services: [
          {
            _id: '59b9598ebd999a00232b7514',
            title: 'Digital Strategy',
            link: 'https://www.onenorth.com/teams/digital-strategy',
            capabilities: [
              'Strategic Planning',
              'Technology Consulting',
              'Data & Optimization Strategy'
            ]
          },
          {
            _id: '59b9598ebd999a00232b7515',
            title: 'Brand & Experience Design',
            link: 'https://www.onenorth.com/teams/digital-strategy',
            capabilities: [
              'Brand Research & Strategy',
              'Content Strategy',
              'UX Strategy & Design',
              'Visual & Interaction Design'
            ]
          },
          {
            _id: '59b9598ebd999a00232b7516',
            title: 'Technology',
            link: 'https://www.onenorth.com/teams/digital-strategy',
            capabilities: [
              'Web Development',
              'Mobile & Custom App Development',
              'Hosting & Application Management',
              'Data Integration'
            ]
          }
        ]
      }
    },
    template: `
      <Hero
        :image="image"
        :title="title"
        :message="message"
        :intro-text="introText"
        :services="services"
      />`
  }))
