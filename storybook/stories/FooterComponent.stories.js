import { storiesOf } from '@storybook/vue'
import FooterComponent from './../../src/components/FooterComponent.vue'
import createStore from './../../src/create-store'

storiesOf('FooterComponent', module)
  .add('default view', () => ({
    components: { FooterComponent },
    data: () => ({
      address: '<p>One North Interactive&nbsp;<br>222 North LaSalle St, #1500 <br>Chicago, IL 60601</p>',
      phoneNumber: '+1 312.469.1740'
    }),
    store: createStore(),
    template: `
      <FooterComponent
        :address="address"
        :phone-number="phoneNumber"
      />
    `
  }))
