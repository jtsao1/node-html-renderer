import { storiesOf } from '@storybook/vue'
import Author from './../../src/components/Author.vue'

storiesOf('Author', module)
  .add('default view', () => ({
    components: { Author },
    data: () => ({
      image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Sheryl-Kravitz_B%26W-Cropped.jpg',
      name: 'Sheryl Kravitz',
      title: 'Director, Brand & Communications',
      content: `<p>Sheryl Kravitz, is Director, Brand and Communications. She has more than 15 years of experience helping highly matrixed companies solve their most pressing brand and communications challenges. Sheryl combines creativity and insight to provide guidance on name development, M&amp;A branding, brand revitalization, portfolio alignment, communications and messaging.</p> <p><strong>Favorite breakfast food</strong>: Coffee and more coffee</p> <p><strong>Favorite Season</strong>: Summer - because the days are longer</p>`,
      socialLinks: [
        {
          link: 'test@onenorth.com',
          icon: 'email'
        },
        {
          link: 'https://www.onenorth.com',
          icon: 'linkedin'
        }
      ]
    }),
    template: `
      <Author
        :image="image"
        :name="name"
        :title="title"
        :content="content"
        :social-links="socialLinks"
      />
    `
  }))
