import { storiesOf } from '@storybook/vue'
import NavMenu from './../../src/components/NavMenu.vue'
import createStore from './../../src/create-store'

storiesOf('NavMenu', module)
  .add('Default View', () => ({
    components: { NavMenu },
    template: `
      <NavMenu
        :is-menu-open="true"
        logo="https://www.onenorth.com/images/logo.svg?5568161e796c8eb48a48b849f5df0a49"
      />
    `,
    store: createStore()
  }))
