import { storiesOf } from '@storybook/vue'
import MenuButton from './../../src/components/MenuButton.vue'

storiesOf('MenuButton', module)
  .add('Default View', () => ({
    components: { MenuButton },
    template: `
      <MenuButton />
    `
  }))
  .add('Is Open', () => ({
    components: { MenuButton },
    template: `
      <MenuButton
        :is-open="true"
        :is-overlay="true"
      />
    `
  }))
