import { storiesOf } from '@storybook/vue'
import TileGrid from './../../src/components/TileGrid.vue'

storiesOf('TileGrid', module)
  .add('Default View', () => ({
    components: { TileGrid },
    data: () => ({
      tiles: [
        {
          logo: 'https://onenorth.blob.core.windows.net/keystone/skadden-black.svg',
          image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Skadden_Hero_Work-Landing.jpg',
          color: '#ed3124',
          link: 'https://www.onenorth.com/work#skadden'
        },
        {
          logo: 'https://onenorthpr.blob.core.windows.net/onenorthpr/plante-black.png',
          image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Plante-Moran-PWA_Hero_Work-Landing.jpg',
          color: '#dc6026',
          link: 'https://www.onenorth.com/work#plante-moran-1'
        },
        {
          logo: 'https://onenorth.blob.core.windows.net/keystone/gt-black.svg',
          image: 'https://tempmigration.blob.core.windows.net/onenorth/GT_Hero_Work-Landing.jpg',
          color: '#be9b39',
          link: 'https://www.onenorth.com/work#greenberg-traurig'
        },
        {
          logo: 'https://onenorth.blob.core.windows.net/keystone/burns-black.png',
          image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Burns_Hero_Work-Landing_New-from-Nate.jpg',
          color: '#0056b9',
          link: 'https://www.onenorth.com/work#burns-and-mcdonnell'
        },
        {
          logo: 'https://onenorth.blob.core.windows.net/keystone/navigant-black.svg',
          image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Navigant_Hero_Work-Landing.jpg',
          color: '#65a337',
          link: 'https://www.onenorth.com/work#navigant'
        },
        {
          logo: 'https://onenorth.blob.core.windows.net/keystone/sidley-black.svg',
          image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Sidley_Hero_Work-Landing.jpg',
          color: '#005e85',
          link: 'https://www.onenorth.com/work#sidley'
        }
      ]
    }),
    template: `
      <TileGrid
        :tiles="tiles"
      />
    `
  }))
