import { storiesOf } from '@storybook/vue'
import HomeHero from './../../src/components/HomeHero.vue'

storiesOf('HomeHero', module)
  .add('default view', () => ({
    components: { HomeHero },
    data () {
      return {
        image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Homepage_Hero-Image_Optimized_12.21.18.jpg',
        title: '<p style="text-align: center;">Digital rede<span style="letter-spacing: .25rem;">f</span>ined</p>',
        message: 'TEKsystems Acquires One North'
      }
    },
    template: `
      <HomeHero
        :image="image"
        :title="title"
        :message="message"
      />`
  }))
