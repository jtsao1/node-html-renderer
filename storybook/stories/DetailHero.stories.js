import { storiesOf } from '@storybook/vue'
import DetailHero from './../../src/components/DetailHero.vue'

storiesOf('DetailHero', module)
  .add('default view', () => ({
    components: { DetailHero },
    data: () => ({
      title: 'How Digital Brand Experiences Transform PSOs and Drive Sustainable Growth',
      date: 'March 04, 2019',
      author: {
        text: 'Sheryl Kravitz',
        url: 'https://www.onenorth.com/blog/?authors=5bd1de01118e86002506f38c'
      },
      image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Anna-Kerinina.jpg'
    }),
    template: `
      <DetailHero
        :title="title"
        :date="date"
        :author="author"
        :image="image"
      />
    `
  }))
