import { storiesOf } from '@storybook/vue'
import SlidingDoors from './../../src/components/SlidingDoors.vue'

storiesOf('SlidingDoors', module)
  .add('default view', () => ({
    components: { SlidingDoors },
    data: () => ({
      featuredItem1: {
        title: 'Ideas',
        header: 'How Digital Brand Experiences Transform PSOs and Drive Sustainable Growth',
        description: 'Are there certain characteristics or traits that high-performing PSOs embody that makes their success look effortless and elegant – traits that underperformers fail to adopt? We think so! High-performing PSOs understand and are able to act on three important and interdependent realities that underperformers tend to struggle with: they lead with brand; they are laser focused on building a remarkable, engaging experience; and they recognize that having a digital engine is critical to their success.',
        buttons: [
          {
            url: 'https://www.onenorth.com/blogs',
            text: 'read this post'
          },
          {
            url: 'https://www.onenorth.com/blogs',
            text: 'view all posts'
          }
        ]
      },
      featuredItem2: {
        title: 'About',
        header: 'Differentiated Digital Experiences',
        description: 'Branded digital experiences not only help organizations stand out in competitive markets, but ultimately win new business, strengthen client relationships, and attract and retain top talent. Our experts use digital to reimagine how clients connect, communicate and engage with their audiences.',
        buttons: [
          {
            url: 'https://www.onenorth.com/about',
            text: 'learn more'
          }
        ]
      }
    }),
    template: `
      <SlidingDoors
        :featured-item1="featuredItem1"
        :featured-item2="featuredItem2"
      />
    `
  }))
