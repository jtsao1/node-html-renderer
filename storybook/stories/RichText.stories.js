import { storiesOf } from '@storybook/vue'
import RichText from './../../src/components/RichText.vue'

storiesOf('RichText', module)
  .add('default view', () => ({
    components: { RichText },
    data () {
      return {
        text: 'Labore voluptate cupidatat duis cupidatat veniam nulla Lorem ipsum esse. Proident ipsum magna sint sint officia fugiat ut veniam aute consequat. Ipsum nostrud anim ullamco duis qui labore anim tempor nostrud enim. Enim occaecat magna sunt ad eu elit. Labore ex ut eiusmod id occaecat consequat nulla pariatur. Tempor sunt sit voluptate et est sit laborum ea. Voluptate in aliqua est mollit qui ipsum veniam aliquip tempor aliquip excepteur id. Duis dolore adipisicing amet esse consequat culpa aliqua adipisicing eiusmod Lorem. Commodo occaecat mollit consequat reprehenderit exercitation commodo officia aliqua quis. Cillum nisi sint quis sit enim cupidatat eiusmod id laboris velit nostrud. Commodo sunt ullamco aute magna veniam ad sunt sint proident veniam nisi nisi.'
      }
    },
    template: `
      <RichText
        :text="text"
      />`
  }))
