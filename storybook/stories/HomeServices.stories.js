import { storiesOf } from '@storybook/vue'
import HomeServices from './../../src/components/HomeServices.vue'

storiesOf('HomeServices', module)
  .add('Default view', () => ({
    components: { HomeServices },
    data: () => ({
      introText: 'Consectetur ea mollit nisi aute non voluptate in incididunt nisi adipisicing sunt. Sit aliqua tempor tempor magna voluptate sint eu culpa anim elit occaecat sint. Lorem ut consectetur in non. Irure deserunt exercitation esse occaecat ut laboris enim occaecat. Aliquip exercitation qui duis consequat pariatur esse laborum labore est commodo consectetur. Nostrud sit aliquip cillum dolore officia. Ipsum officia ex commodo pariatur dolore laborum esse do ullamco laborum enim irure ut.',
      services: [
        {
          _id: '59b9598ebd999a00232b7514',
          title: 'Digital Strategy',
          link: 'https://www.onenorth.com/teams/digital-strategy',
          capabilities: [
            'Strategic Planning',
            'Technology Consulting',
            'Data & Optimization Strategy'
          ]
        },
        {
          _id: '59b9598ebd999a00232b7515',
          title: 'Brand & Experience Design',
          link: 'https://www.onenorth.com/teams/digital-strategy',
          capabilities: [
            'Brand Research & Strategy',
            'Content Strategy',
            'UX Strategy & Design',
            'Visual & Interaction Design'
          ]
        },
        {
          _id: '59b9598ebd999a00232b7516',
          title: 'Technology',
          link: 'https://www.onenorth.com/teams/digital-strategy',
          capabilities: [
            'Web Development',
            'Mobile & Custom App Development',
            'Hosting & Application Management',
            'Data Integration'
          ]
        }
      ]
    }),
    template: `
      <div style="background-color: black">
        <HomeServices
          :intro-text="introText"
          :services="services"
        />
      </div>
    `
  }))
