import { storiesOf } from '@storybook/vue'
import SiteNav from './../../src/components/SiteNav.vue'
import createStore from './../../src/create-store'

storiesOf('SiteNav', module)
  .add('default view', () => ({
    components: { SiteNav },
    data () {
      return {
        image: 'https://onenorthpr.blob.core.windows.net/onenorthpr/Homepage_Hero-Image_Optimized_12.21.18.jpg',
        title: '<p style="text-align: center;">Digital rede<span style="letter-spacing: .25rem;">f</span>ined</p>',
        message: 'TEKsystems Acquires One North'
      }
    },
    store: createStore(),
    template: `
      <SiteNav
        :image="image"
        :title="title"
        :message="message"
      />`
  }))
