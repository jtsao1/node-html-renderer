import { storiesOf } from '@storybook/vue'
import BrandLink from './../../src/components/BrandLink.vue'

storiesOf('BrandLink', module)
  .add('Dark Theme', () => ({
    components: { BrandLink },
    data: () => ({
      to: 'https://www.onenorth.com'
    }),
    template: `
      <BrandLink
        :to="to"
        theme="dark"
      >
        Dark Theme
      </BrandLink>`
  }))
  .add('White Theme', () => ({
    components: { BrandLink },
    template: `
      <BrandLink
        :to="to"
        theme="white"
      >
        White Theme
      </BrandLink>
    `
  }))
