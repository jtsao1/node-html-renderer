const path = require('path')
const storybook = require('@storybook/vue/standalone')
const { storybookOutputPath } = require('./../project-config.js')

storybook({
  mode: process.env.STORYBOOK_MODE === 'static' ? 'static' : 'dev',
  port: parseInt(process.env.PORT || process.env.STORYBOOK_PORT),
  staticDir: [
    path.join(__dirname, '../src/static')
  ],
  configDir: __dirname,
  outputDir: storybookOutputPath,
  quiet: true
})
